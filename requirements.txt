requests==2.24.0
json5==0.9.1
jsonschema==3.2.0
Flask==1.1.2
flask-restx==0.2.0
flask-cors
pymongo
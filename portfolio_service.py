import logging
import mongo_doa as doa
from mongo_doa import mydb as db
LOG = logging.getLogger(__name__)

class Portfolio:
     # These methods will access DB
    def __init__(self):
        # Create Robot & User portfolio
        robot = {"_id": "001", "cash": 5000, "stocks": {}}
        user = {"_id": "002", "cash": 5000, "stocks": {}}
        db.portfolios.remove({})
        db.portfolios.insert_one(robot)
        db.portfolios.insert_one(user)

    def get_portfolio_by_id(self, id):
        portfolio = db.portfolios.find_one({"_id": id})
        return portfolio

    # "AAPL": {"Price": 000, "Action" : 1}
    def update_portfolio(self, trade, user_id):
        current_portfolio = self.get_portfolio_by_id(user_id)
        key = trade[0]
        value = trade[1]

        if value["Action"] == -1: #SELL
            if key in current_portfolio["stocks"]:
                current_portfolio["cash"] += value["Price"]
                del current_portfolio["stocks"][key]
        else: #BUY
            if key not in current_portfolio["stocks"]:
                current_portfolio["stocks"][key] = 1
                current_portfolio["cash"] -= value["Price"]
        db.portfolios.save(current_portfolio)

    def accept_trades(self, trades, user_id): # The list of filled trades
        for trade in trades.items():
            self.update_portfolio(trade, user_id)
        
    def get_balance(self, user_id):
        portfolio = self.get_portfolio_by_id(user_id)
        return portfolio["cash"]

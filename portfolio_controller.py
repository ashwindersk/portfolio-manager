from datetime import datetime
import logging
import flask_cors

from flask import Flask, request
from flask_restx import Resource, Api, fields
from portfolio_service import Portfolio as ps 

LOG = logging.getLogger(__name__)

app = Flask(__name__)
api = Api(app)
flask_cors.CORS(app, resources=r'/*')

ns=api.namespace("v1/portfolio", description = "Getting portfolio and Updating portfolio")

#TODO Get from current account

trade_portfolio = api.model('Portfolio', 
    {
        'user_id': fields.String(required=True),
        'trades': fields.Raw(required=True, example={"AAPL":{"Price":10,"Action":-1},
                                                        "TSLA":{"Price":200,"Action":1}}) 
    })


ps = ps()

@ns.route("/")
class Portfolio_controller(Resource):
    @ns.expect(trade_portfolio)
    def post(self):
        LOG.info("Receiving trades" + str(api.payload["trades"]))

        # Parse out input
        today_trades = api.payload['trades']
        user_id = api.payload['user_id']

        LOG.info("Updating portfolio...")
        ps.accept_trades(today_trades, user_id)
        LOG.info("Portfolio updated...")
        
        return ps.get_portfolio_by_id(user_id)

@ns.route('/<string:user_id>')
@ns.param('user_id', 'An example path variable')
class Example(Resource):
    def get(self, user_id):
        LOG.info('Getting portfolio...')
        LOG.info(str(user_id))
        return ps.get_portfolio_by_id(user_id)

@ns.route('/balance/<string:user_id>')
@ns.param('user_id', 'An example path variable')
class Example(Resource):
    def get(self, user_id):
        LOG.info('Getting balance')
        LOG.info(str(user_id))
        return ps.get_balance(user_id)

        
if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    app.run(debug=True, host="0.0.0.0")
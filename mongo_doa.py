import os
import pymongo
from pymongo import MongoClient

# Enviroment variables 
# os.environ['MONGO_DB'] = "localhost:27017"
MONGO = os.getenv("MONGO_DB", "localhost:27017")
# MONGO = os.environ.get('MONGO_DB')

myclient = MongoClient("mongodb://" + MONGO)

mydb = myclient["users"]